import React, { Component } from "react";
import {
  Button,
  Segment,
  Dimmer,
  Loader,
  Container,
  Card
} from "semantic-ui-react";
import "./index.css";
import { init } from "../../utilities/firebase";
import LoginForm from "../../components/auth/LoginForm";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

class ChatComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.authUser) {
      return <Redirect to="/" />;
    }

    if (
      !this.props.onlineUsers ||
      (this.props.onlineUsers && !Object.keys(this.props.onlineUsers).length)
    ) {
      return (
        <div>
          <Segment>
            <Dimmer active inverted>
              <Loader inverted>waiting for Online users</Loader>
            </Dimmer>
            <div style={{ height: "200px" }} />
          </Segment>
        </div>
      );
    }

    const items = Object.keys(this.props.onlineUsers).map(key => ({
      header: `Anonymous`,
      description: `Email: ${this.props.onlineUsers[key].email}`,
      meta: `ID: ${this.props.onlineUsers[key].uid}`,
      link: true,
      onClick: () => {
        this.props.history.push("chat/" + this.props.onlineUsers[key].uid);
      }
    }));

    return (
      <Container style={{ padding: "40px 10px" }}>
        <Card.Group items={items} />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  authUser: state.auth.user,
  onlineUsers: state.chat.onlineUsers
});

export default connect(mapStateToProps)(ChatComponent);
