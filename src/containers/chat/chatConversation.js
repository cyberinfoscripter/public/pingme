import React, { Component } from "react";
import {
  Button,
  Segment,
  Dimmer,
  Loader,
  Container,
  Card,
  Header,
  Image,
  Input,
  Form,
  TextArea
} from "semantic-ui-react";
import "./index.css";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { width } from "window-size";

class ChatConversationComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatList: [],
      textarea: {
        value: ""
      }
    };
  }

  sentFormHandler = e => {
    e.preventDefault();
  };

  render() {
    return (
      <Container style={{ padding: "40px 10px" }}>
        <Header as="h2">
          <Image
            circular
            src="http://icons.iconarchive.com/icons/seanau/user/72/Thief-icon.png"
          />
          <p>
            User :- <br />
            <small>{this.props.match.params.uid}</small>
          </p>
        </Header>
        <Segment
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              height: "500px",
              border: "1px solid #ccc"
            }}
          >
            <div style={{ flex: 1 }}>
              <p>Chat list</p>
            </div>
            <div>
              <Form
                onSubmit={this.sentFormHandler}
                style={{ display: "flex", flexDirection: "row" }}
              >
                <TextArea
                  value={this.state.textarea.value}
                  onChange={e => {
                    console.log(e.keyCode);

                    let updatedValue = e.target.value;

                    this.setState(state => {
                      const updatedState = {
                        ...state,
                        textarea: {
                          ...this.state.textarea,
                          value: updatedValue
                        }
                      };

                      return updatedState;
                    });
                  }}
                  style={{ resize: "none" }}
                  placeholder="Tell us more"
                />
                <Button>Sent</Button>
              </Form>
            </div>
          </div>
        </Segment>
      </Container>
    );
  }
}

const style = {
  innerDiv: {}
};

const mapStateToProps = state => ({
  authUser: state.auth.user,
  onlineUsers: state.chat.onlineUsers
});

export default connect(mapStateToProps)(ChatConversationComponent);
