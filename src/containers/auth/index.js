import React, { Component } from "react";
import { Button } from "semantic-ui-react";
import "./auth.css";
import { init } from "../../utilities/firebase";
import { auth } from "firebase";
import LoginForm from "../../components/auth/LoginForm";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

class Auth extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    return this.props.authUser ? <Redirect to="/chat" /> : <LoginForm />;
  }
}

const mapStateToProps = state => ({
  authUser: state.auth.user
});

export default connect(mapStateToProps)(Auth);
