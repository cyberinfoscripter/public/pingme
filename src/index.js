import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import authReducer from "./store/reducer/auth";
import chatReducer from "./store/reducer/chat";
import Firebase from "./utilities/firebase";
import Socket from "./utilities/socket";
window.socket = null;

const middelwareLogger = store => {
  return next => {
    return action => {
      const result = next(action);

      console.log("[middelwareLogger]", store.getState());

      return result;
    };
  };
};
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  auth: authReducer,
  chat: chatReducer
});

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(middelwareLogger, thunk))
);

const app = (
  <Provider store={store}>
    <Fragment>
      <Socket />
      <Firebase />
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Fragment>
  </Provider>
);

ReactDOM.render(app, document.getElementById("root"));
registerServiceWorker();
