import React, { Component } from "react";
import { Button, Container, Input } from "semantic-ui-react";
import "./index.css";
import { init, FirebaseComponent } from "../../utilities/firebase";
import { auth } from "firebase";
import { connect } from "react-redux";
import * as actionTypes from "../../store/actions/actionTypes";
import SocketIOClient from "socket.io-client";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailInput: {
        value: ""
      },
      passwordInput: {
        value: ""
      }
    };
  }

  loginFormSubmit = e => {
    e.preventDefault();
    let requestPayload = {
      email: this.state.emailInput.value,
      password: this.state.passwordInput.value
    };

    FirebaseComponent.loginRequest(requestPayload)
      .then(result => {
        console.log(result);

        const user = {
          uid: result.user.uid,
          displayName: result.user.displayName,
          email: result.user.email
        };

        console.log(user);

        console.log(this.props.actionDispatchUser);
        this.props.actionDispatchUser(user);

        //console.log(result.user.getUid());
      })
      .catch(error => {
        this.setState(state => ({
          ...this.state,
          emailInput: { ...this.state.emailInput, value: "" },
          passwordInput: { ...this.state.passwordInput, value: "" }
        }));

        alert("Invalid Credentials");
      });
  };

  render() {
    return (
      <Container>
        <div className="auth-login-container">
          <form
            className="auth-login-container-form"
            onSubmit={this.loginFormSubmit}
          >
            <Input
              style={inlineStyles.inputEmail}
              placeholder="Email"
              maxLength={60}
              value={this.state.emailInput.value}
              onChange={e => {
                let inputEmailValue = e.target.value;
                this.setState(state => ({
                  ...state,
                  emailInput: {
                    ...this.state.emailInput,
                    value: inputEmailValue
                  }
                }));
              }}
            />
            <Input
              style={inlineStyles.inputPassword}
              type="password"
              placeholder="Password"
              value={this.state.passwordInput.value}
              maxLength={20}
              onChange={e => {
                let inputPasswordValue = e.target.value;
                this.setState(state => ({
                  ...state,
                  passwordInput: {
                    ...this.state.passwordInput,
                    value: inputPasswordValue
                  }
                }));
              }}
            />
            <Button type="submit">Login</Button>
          </form>
        </div>
      </Container>
    );
  }
}

let inlineStyles = {
  inputEmail: { margin: "10px 10px 10px 10px" },
  inputPassword: { margin: "0px 10px 10px 10px" }
};

const mapStateToProps = state => ({
  authUser: state.auth.user
});

const mapDispatchToProps = dispatch => ({
  actionDispatchUser: user => {
    return dispatch({
      type: actionTypes.AUTH_ADD_USER,
      payload: {
        user: user
      }
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);
