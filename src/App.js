import React, { Component } from "react";
import "./App.css";
import AuthContainer from "./containers/auth/";
import ChatContainer from "./containers/chat/";
import chatConversation from "./containers/chat/chatConversation";
import "semantic-ui-css/semantic.min.css";
import { Switch, Route } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img
            src={
              "http://icons.iconarchive.com/icons/seanau/flat-app/256/Chats-icon.png"
            }
            className="App-logo"
            alt="logo"
          />
          <h1 className="App-title">PingMe</h1>
          <small>What about doing a Instant chat :)</small>
        </header>
        <section>
          <Switch>
            <Route path="/chat/:uid" exact component={chatConversation} />
            <Route path="/chat" exact component={ChatContainer} />
            <Route path="/" exact component={AuthContainer} />
          </Switch>
        </section>
      </div>
    );
  }
}

export default App;
