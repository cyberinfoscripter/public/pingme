import * as actionsTypes from "../actions/actionTypes";
import cloneDeep from "lodash/cloneDeep";

const initialState = {
  onlineUsers: []
};

const reducer = (state = initialState, action) => {
  let updated_state = cloneDeep(state);

  switch (action.type) {
    case actionsTypes.CHAT_ADD_ONLINE_USERS: {
      updated_state.onlineUsers = action.payload.onlineUsers;

      return updated_state;
    }
    default:
      return state;
  }
};

export default reducer;
