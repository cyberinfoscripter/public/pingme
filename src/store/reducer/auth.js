import * as actionsTypes from "../actions/actionTypes";
import cloneDeep from "lodash/cloneDeep";

const initialState = {
  user: null
};

const reducer = (state = initialState, action) => {
  let updated_state = cloneDeep(state);

  switch (action.type) {
    case actionsTypes.AUTH_ADD_USER: {
      updated_state.user = action.payload.user;

      return updated_state;
    }
    default:
      return state;
  }
};

export default reducer;
