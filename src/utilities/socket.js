import * as firebase from "firebase";
import React, { Component } from "react";
import { connect } from "react-redux";
import SocketIOClient from "socket.io-client";
import * as actionTypes from "../store/actions/actionTypes";

class SocketComponent extends Component {
  static getDerivedStateFromProps(props, state) {
    console.log("getDerivedFromProps", props, state);
    let returnState = { ...state };

    if (props.authUser && !state.authUser) {
      returnState.authUser = props.authUser;
      returnState.initiateSocket = true;
    } else if (returnState.initiateSocket) {
      returnState.initiateSocket = false;
    }

    return returnState;
  }

  constructor(props) {
    super(props);
    this.state = {
      authUser: null,
      initiateSocket: false
    };
  }

  componentDidUpdate() {
    if (this.state.initiateSocket) {
      //http://0e2af3d0.ngrok.io

      this.socket = window.socket = SocketIOClient("http://e79e6d53.ngrok.io", {
        query: {
          user: JSON.stringify(this.props.authUser)
        }
      });
      this.socket
        .on("connect", () => {
          this.socket.emit("GET_ONLINE_USERS", {}, result => {
            console.log("GET_ONLINE_USERS : ", result);

            if (Object.keys(result).length) {
              delete result[this.state.authUser.uid];

              this.props.dispatchUpdateOnlineUsers(result);
            }
          });
        })
        .on("disconnect", () => false)
        .on("NEW_MESSAGE_FROM_USER", ({ data }) => {
          console.log("new message from user");
        });
    }
  }

  render() {
    return null;
  }
}

export { SocketComponent };

const mapStateToProps = state => ({
  authUser: state.auth.user
});

const mapDispatchToProps = dispatch => ({
  dispatchUpdateOnlineUsers: users => {
    return dispatch({
      type: actionTypes.CHAT_ADD_ONLINE_USERS,
      payload: {
        onlineUsers: users
      }
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SocketComponent);

// this.socket = Config.SOCKET = SocketIOClient("http://29893121.ngrok.io", {
//   query: {
//     user: this.props.appUser.id
//   }
// });
// this.socket
//   .on("connect", () => this.props.dispatchAppSocketConnectionStatus(true))
//   .on("disconnect", () => this.props.dispatchAppSocketConnectionStatus(false))
//   .on("API_REQUEST_MESSAGE_GET_USERS_RESULT", ({ data }) => {
//     const payload = { data };
//     this.props.dispatchApiRequestMessageGetUsersResult(payload);
//   });
