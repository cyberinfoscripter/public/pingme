import * as firebase from "firebase";
import React, { Component } from "react";
import { connect } from "react-redux";

class FirebaseComponent extends Component {
  static loginRequest = ({ email, password }) => {
    console.log("loginRequest", email, password);

    return firebase
      .auth()
      .setPersistence(firebase.auth.Auth.Persistence.SESSION)
      .then(() => {
        return firebase.auth().signInWithEmailAndPassword(email, password);
      });
  };

  constructor(props) {
    super(props);
    let config = {
      apiKey: "AIzaSyCVyEKd4lVuGyIpOQlSaLVp4vYY9BQUmjE",
      authDomain: "pingme-coder.firebaseapp.com",
      databaseURL: "https://pingme-coder.firebaseio.com",
      projectId: "pingme-coder",
      storageBucket: "pingme-coder.appspot.com",
      messagingSenderId: "79604702522"
    };
    firebase.initializeApp(config);
    // database = firebase.database();
  }

  componentDidMount() {
    console.log("Firebaseuser : ", firebase.auth().currentUser);
  }

  render() {
    return null;
  }
}

export { FirebaseComponent };

const mapStateToProps = state => ({});
// const mapDispatchToProps = dispatch => ({

// });

export default connect(mapStateToProps)(FirebaseComponent);
